require 'mumble-ruby'

# Configure all clients globally
Mumble.configure do |conf|
    # sample rate of sound (48 khz recommended)
    conf.sample_rate = 48000

    # bitrate of sound (32 kbit/s recommended)
    conf.bitrate = 32000

    # directory to store user's ssl certs
    conf.ssl_cert_opts[:cert_dir] = File.expand_path("./")
end

# Create client instance for your server
cli = Mumble::Client.new('localhost') do |conf|
    conf.username = 'Jukebot'
    conf.password = ''
    conf.port     = 36161

    # Overwrite global config
    conf.bitrate = 48000
end
# => #<Mumble::Client:0x00000003064fe8 @host="localhost", @port=64738, @username="Mumble Bot", @password="password123", @channels={}, @users={}, @callbacks={}>

# Set up some callbacks for when you recieve text messages
# There are callbacks for every Mumble Protocol Message that a client can recieve
# For a reference on those, see the linked PDF at the bottom of the README.
cli.on_text_message do |msg|
    puts msg.message
end
# => [#<Proc:0x0000000346e5f8@(irb):2>]

# Initiate the connection to the client
cli.connect
# => #<Thread:0x000000033d7388 run>

# Mute and Deafen yourself after connecting
cli.on_connected do
    cli.me.mute
    cli.me.deafen
end

# Join the channel titled "Chillen" (this will return a channel object for that channel)
cli.join_channel('Dota 2')

# Get a list of channels
cli.channels
# Returns a hash of channel_id: Channel objects

# Join Channel using ID
#cli.join_channel(0)

# Join Channel using Channel object
#cli.join_channel(cli.channels[0])
#cli.channels[0].join

# Get a list of users
puts cli.users
# Returns a hash of session_id: UserState Messages

# Text user
#cli.text_user('perrym5', "Hello there, I'm a robot!")

# Text an image to a channel
#cli.text_channel_img('Chillen', '/path/to/image.jpg')

# Start streaming from a FIFO queue of raw PCM data
#cli.player.stream_named_pipe('/tmp/mpd.fifo')

# EXPERIMENTAL: Recording feature
#cli.recorder.start('/home/matt/record.wav')
#sleep(2)
#cli.recorder.stop

# EXPERIMENTAL: Play wav files
sleep 5
cli.player.play_file('./join.wav')

# Safely disconnect
cli.disconnect
# => nil